cmake_minimum_required(VERSION 2.8)
project(HSVSliders)
find_package(OpenCV REQUIRED)
include_directories(${OpenCV_INCLUDE_DIRS})
add_executable(HSVSliders main.cpp)
target_link_libraries(HSVSliders ${OpenCV_LIBS} )

