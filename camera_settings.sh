#!/bin/bash
v4l2-ctl -d /dev/video0 --set-ctrl=brightness=60
v4l2-ctl -d /dev/video0 --set-ctrl=contrast=0

v4l2-ctl -d /dev/video0 --set-ctrl=saturation=50
v4l2-ctl -d /dev/video0 --set-ctrl=sharpness=50
v4l2-ctl -d /dev/video0 --set-ctrl=rotate=180


v4l2-ctl -d /dev/video0 --set-ctrl=color_effects=0

v4l2-ctl -d /dev/video0 --set-ctrl=auto_exposure=0
v4l2-ctl -d /dev/video0 --set-ctrl=exposure_time_absolute=1000
v4l2-ctl -d /dev/video0 --set-ctrl=image_stabilization=0
v4l2-ctl -d /dev/video0 --set-ctrl=iso_sensitivity=4
v4l2-ctl -d /dev/video0 --set-ctrl=iso_sensitivity_auto=0


v4l2-ctl -d /dev/video0 --set-ctrl=compression_quality=10
v4l2-ctl -d /dev/video0 --set-ctrl=white_balance_auto_preset=7
