//------------------------------------------------
// project:     Image processing 1 project   
// file:    	shapeDetection.cpp
// school:      FHGR
// authors:     Lukas Marti
//              Deborah Schrag
// date:        09.04.2023
// version:     1.0
// toolchain:	Editor:     vim
//		        Compiler:	gcc
//		        Builder:	Cmake
//		        OS:		    Linux (Pop_OS 22.04)
//		                    Raspberry Pi OS
//
// brief:      Functions for the shape detector 
//------------------------------------------------

#include "../inc/shapeDetection.hpp"

//-----------------------------
//Constants
//----------------------------
const shapeDetection::colors_hsv_t LIGHT_BLUE = {64, 35, 0, 101, 255, 255}; 
const shapeDetection::colors_hsv_t VIOLET = {112, 40, 0, 161, 185, 255}; 
const shapeDetection::colors_hsv_t ORANGE = {2, 83, 50, 22, 255, 255};

//-----------------------------
//Functions
//----------------------------
shapeDetection::shapeDetection(cv::Mat img_in)
{
    img_in.copyTo(imgOrig);
}

shapeDetection::shapeDetection(void)
{
}

shapeDetection::~shapeDetection()
{
}

void shapeDetection::newFrame(cv::Mat img_in)
{
    img_in.copyTo(imgOrig);
    imgMasked.release();
}

cv::Mat shapeDetection::createMask(colors_hsv_t color)
{
    imgOrig.copyTo(imgHSV);
    blur(imgHSV, 3);
    cv::imshow("After Blurring", imgHSV);

    cv::cvtColor(imgHSV, imgHSV, cv::COLOR_BGR2HSV);
    cv::inRange(imgHSV, cv::Scalar(color.min_H, color.min_S, color.min_V), 
    cv::Scalar(color.max_H, color.max_S, color.max_V), mask);
    //cv::Mat kernel = cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(100, 100));


    //Failed Experiment to improve masking:

    //Dilate Mask (create extra buffer around it)
    /*cv::Mat dilate_kernel = getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(30, 30));
    cv::dilate(mask, mask, dilate_kernel);

    //Create Blurred Mask edges
    cv::Mat imgMaskedFeather;
    img.copyTo(imgMaskedFeather, feather);
    cv::blur(imgMaskedFeather, imgMaskedFeather, cv::Size(30, 30));
    cv::imshow("feathered mask", imgMaskedFeather);*/

    //Apply and blend masks (Doesn't work well yet -> imgMasked is used in next operations)
    imgOrig.copyTo(imgMasked, mask);
    cv::imshow("masked image", imgMasked);
    //imgMasked.copyTo(imgMaskedFeather, imgMask);
    cv::imshow("mask", mask);

    return mask;
}

cv::Mat shapeDetection::createCanny(cv::Mat img_in)
{
    cv::Mat img;
    img = img_in.clone();

    cv::cvtColor(img, img, cv::COLOR_BGR2GRAY);

    img.convertTo(img, -1, 2, 0);
    cv::imshow("Adjusted contrast", img);

    cv::blur(img, img , cv::Size(3, 3));
    cv::Canny(img, imgEdge, 0, 100);
    cv::imshow("canny-edge", imgEdge);

    return imgEdge;
}

bool shapeDetection::cubeDetection()
{
    findLines(imgEdge);
    std::cout << "Number of lines: " << lines.size() << std::endl;

    if(lines.size() >= 4 && lines.size() <= 9)
    {
        cubeDetected = 1;
        std::cout <<  "Cube detected" << std::endl;
        return 1;
    }
    else
    {
        cubeDetected = 0;
        std::cout << "no Cubes :(" << std::endl;
        return 0;
    }
}


bool shapeDetection::circleDetection()
{
    findCircles(imgMasked);    

    if(circles.size() == 1)
    {
        ballDetected = 1;
        std::cout << "Ball detected" << std::endl;
        return 1;
    }
    else
    {
        ballDetected = 0;
        std::cout << "No Ball detected :(" << std::endl;
        return 0;
    }
}


void shapeDetection::blur(cv::Mat img_in, uint16_t kernel_size)
{
    cv::blur(img_in, img_in , cv::Size(kernel_size, kernel_size));
    cv::Mat img_bil;
    cv::bilateralFilter(img_in, img_bil, 7, 200, 200, cv::BORDER_DEFAULT);
    cv::medianBlur(img_bil, img_in, 5);
    cv::dilate(img_in, img_in, 5);
}


void shapeDetection::findLines(cv::Mat img_in)
{
    lines.clear();
    cv::HoughLinesP(img_in, lines, 1, CV_PI/180, 50, 20, 500);
}


void shapeDetection::findCircles(cv::Mat img_in)
{   
    circles.clear();
    cv::HoughCircles(imgEdge, circles, cv::HOUGH_GRADIENT, 2, 500, 200, 100);
}


cv::Mat shapeDetection::showLines(void)
{
    imgLines = imgOrig.clone();
    std::string text;

    if(cubeDetected == 1)
    {
        text = "Cube detected";
    }
    else
    {
        text = "No Cube detected";
    }

    if(lines.size() > 0)
    {
        for(size_t i = 0; i < lines.size(); i++)
        {
            cv::Vec4i l = lines[i];
            line(imgLines, cv::Point(l[0], l[1]), cv::Point(l[2], l[3]), cv::Scalar(0, 255, 0), 2, cv::LINE_AA);
        }
    }
    cv::putText(imgLines, text, cv::Point(80, 80), cv::FONT_HERSHEY_COMPLEX, 1, cv::Scalar(0, 0, 255), 2);
    cv::imshow("detected lines", imgLines);

    return imgLines;
}

cv::Mat shapeDetection::showCircles(void)
{
    imgCircles = imgOrig.clone();

    std::string text;

    if(ballDetected == 1)
    {
        text = "Ball detected";
    }
    else
    {
        text = "No Ball detected";
    }

    cv::putText(imgCircles, text, cv::Point(80, 80), cv::FONT_HERSHEY_COMPLEX, 1, cv::Scalar(0, 0, 255), 2);

    
    if(circles.size() > 0)
    {
        for(size_t i = 0; i < circles.size(); i++)
        { 
            cv::Vec3i c = circles[i];
            cv::Point center = cv::Point(c[0], c[1]);
            int radius = c[2];

            circle(imgCircles, center, radius, cv::Scalar(255,0,255), 3, cv::LINE_AA);
            std::cout << lines.size() << std::endl;
        }
    }

    cv::imshow("detected circles", imgCircles);

    return imgCircles;
}


cv::Mat shapeDetection::captureImage(cv::Mat img_in, std::string windowName)
{
    std::cout << "Picture taken" << std::endl;
    cv::imshow(windowName, img_in);

    newFrame(img_in);
    createMask(VIOLET);
    createCanny(imgMasked);
    
    cubeDetection();
    showLines();
    circleDetection();
    showCircles();

    return img_in;
}

