//------------------------------------------------
// project:     Image processing 1 project   
// file:    	shapeDetection.hpp
// school:      FHGR
// authors:     Lukas Marti
//              Deborah Schrag
// date:        09.04.2023
// version:     1.0
// toolchain:	Editor:     vim/VSCode
//		        Compiler:	gcc
//		        Builder:	Cmake
//		        OS:		    Linux (Pop_OS 22.04)
//		                    Raspberry Pi OS
//
// brief:       Class Definitions for the shape
//              detector
//------------------------------------------------

#pragma once

#include <string>
#include <iostream>
#include <vector>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/photo.hpp>


//-----------------------------
//Defines
//----------------------------
#define LOW_THRESHOLD 50 
#define RATIO 3

/**
 * @brief Shape Detection Class
 *
 * Includes all the necessary functions and variables to 
 * detect rudimentary shapes on a Raspberry Pi
 */
class shapeDetection
{
    public:
        /** @struct colors_hsv_t
         *   Struct to store color ranges
         */
        typedef struct  
        {
            uint16_t min_H; //Minimum Hue
            uint16_t min_S; //Minimum Saturation
            uint16_t min_V; //Minimum Vibrance
            uint16_t max_H; //Maximum Hue
            uint16_t max_S; //Maximum Saturation
            uint16_t max_V; //Maximum Vibrance
        }colors_hsv_t;


    public:
        /**
         * @brief Constructor
         *
         * @param img_in Initial frame
         */
        shapeDetection(cv::Mat img_in);

        /**
         *  @brief Constructor
         */
        shapeDetection(void);

        /** 
         *  @brief Destructor
         */
        ~shapeDetection();
        
        /**
         * @brief insert new frame
         *
         * @param img_in new frame
         */
        void newFrame(cv::Mat img_in);

        /**
         * @brief Create color based mask
         *
         * @param color Color to be masked by
         *
         * @return masked image
         */
        cv::Mat createMask(colors_hsv_t color);

        /**
         * @brief Apply Highpass filter to extract edges
         *
         * @param img_in Image to extract edges
         *
         * @return Canny processed image 
         */
        cv::Mat createCanny(cv::Mat img_in);

        /**
         * @brief Evaluates, whether a cube is in the image or not
         *
         * @return True if cube is detected, false if not
         */
        bool cubeDetection();

        /**
         * @brief Evaluaties, whether a ball is in the image or not
         *
         * @return True if ball is detected, false if not
         */
        bool circleDetection();

        /**
         * @brief shows the lines on output image
         * 
         * @return Mat with lines
         */
        cv::Mat showLines();

        /**
         * @brief shows the circles on output image
         *
         * @return Mat with circles
         */
        cv::Mat showCircles();

        cv::Mat captureImage(cv::Mat img_in, std::string windowName);

        
    public:
        bool cubeDetected = 0;
        bool ballDetected = 0;


    private:
        /**
         * @brief Combination of Filters
         *        Best combination of filters to prepare a noisy image
         *        from a cheap camera for shape detection
         *
         * @param img_in input image
         * @param kernel_size size of the kernel when filtering
         */
        void blur(cv::Mat img_in, uint16_t kernel_size);

        /**
         * @brief Finds lines with hough transformation
         *
         * @param img_in input image
         */
        void findLines(cv::Mat img_in);
        
        /**
         * @brief Finds cirlces with hough transformation
         *
         * @param img_in inpu image
         */
        void findCircles(cv::Mat img_in);
           
           
    priva  te:
        cv::Mat imgOrig;        //Original image
        cv::Mat mask;           //B&W mask
        cv::Mat imgMasked;      //Masked, colored image
        cv::Mat imgGray;        //Grayscale image, after masking
        cv::Mat imgHSV;         //Masked, colored image in HSV color spectrum
        cv::Mat imgEdge;        //Image after HighPass
        cv::Mat imgLines;       //Image with drawn on lines
        cv::Mat imgCircles;     //Image with drawn on circles

        std::vector<cv::Vec4i> lines;   //Vector to store all lines
        std::vector<cv::Vec3f> circles; //Vector to store all circles
};

//Color ranges for the different object colors
extern const shapeDetection::colors_hsv_t LIGHT_BLUE; 
extern const shapeDetection::colors_hsv_t ORANGE;
extern const shapeDetection::colors_hsv_t VIOLET;
