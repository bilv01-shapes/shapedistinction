//------------------------------------------------
// project:     Image processing 1 project   
// file:    	main.cpp
// school:      FHGR
// authors:     Lukas Marti
//              Deborah Schrag
// date:        09.04.2023
// version:     1.0
// toolchain:	Editor:     vim
//		        Compiler:	gcc
//		        Builder:	Cmake
//		        OS:		    Linux (Pop_OS 22.04)
//
// brief:       Testing the shape detection on a        
//              linux device, to later port to 
//              Raspberry Pi
//------------------------------------------------

#include <string>
#include <iostream>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>

#include "inc/shapeDetection.hpp"


//-----------------------------
//Defines
//----------------------------
#define FPS 10


//-----------------------------
//Main 
//----------------------------
int main(int argc, char* argv[])
{
    std::string windowName = "Input Image";

    cv::VideoCapture capture; 
    capture.open(0);
    
    shapeDetection detector;
    cv::Mat frame;
    char c;

    capture >> frame;
    cv::namedWindow(windowName, cv::WINDOW_AUTOSIZE);
    cv::imshow(windowName, frame);

    
    while(1)
    {
        capture >> frame;

        if(c == 'p')    //When "p" gets pressed
        {
            detector.captureImage(frame, windowName);   //Take new frame
        }   

        if (c == 27)    //When escape gets pressed
        {
            break;  //Exit
        }

        c = cv::waitKey(1000. / FPS);
    }

    return(0);
}

