# shapeDistinction

This program is able to see and destinguish between various object shapes and colours. It relies on c++ and OpenCV.

Its intended use is as part of a ROS-running Raspberry Pi 4b, which controls a robot, built for the Mobile Robotics Project.

```
I'm in love with the shape of you
As long as it resembles a cube
Even a round ball would be good
I'm in love with your outlines`
```

